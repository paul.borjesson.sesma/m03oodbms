package com.company;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import java.time.LocalDateTime;
import java.util.List;

public class Encarrec {

    public static void menu2(ObjectContainer db) {
        Encarrec_Dades[] encarrecs = {
                new Encarrec_Dades(0, 1, LocalDateTime.now()),
                new Encarrec_Dades(1, 1, LocalDateTime.now()),
                new Encarrec_Dades(2, 3, LocalDateTime.now()),
                new Encarrec_Dades(3, 1, LocalDateTime.now()),
                new Encarrec_Dades(4, 1, LocalDateTime.now()),
                new Encarrec_Dades(5, 4, LocalDateTime.now()),
                new Encarrec_Dades(6, 3, LocalDateTime.now())
        };

        for (Encarrec_Dades e : encarrecs) {
            db.store(e);
        }
    }

    public static void menu11(ObjectContainer db) {
        Predicate p = new Predicate<Encarrec_Dades>() {
            @Override
            public boolean match(Encarrec_Dades encarrec_dades) {       //Retorna tots els encarrec_dades
                return true;
            }
        };

        List<Encarrec_Dades> encarrecs = db.query(p);

        for(Encarrec_Dades e_dades : encarrecs) {
            System.out.println(e_dades.toString());
        }

//        ObjectSet<Encarrec_Dades> result = db.query(p);
//
//        while (result.hasNext()) {
//            Encarrec_Dades encarrecTmp = result.next();
//            System.out.println(encarrecTmp.toString());
//        }
    }

}
