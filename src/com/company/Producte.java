package com.company;

import com.db4o.ObjectContainer;
import com.db4o.query.Predicate;

import java.util.List;

public class Producte {

    public static void menu1(ObjectContainer db) {
        Producte_Dades[] productes = {
                new Producte_Dades(1, "Ració de combat", 1, 0),
                new Producte_Dades(2, "Torpede mark 2", 100, 2),
                new Producte_Dades(3, "Autopilot SAU-6", 10, 3),
                new Producte_Dades(4, "Sistema inercial MIS-P", 11, 3),
                new Producte_Dades(5, "Bobina d'inducció magnètica del reactor principal", 10000, 4),
                new Producte_Dades(6, "Sistema de navegació de llarg abast RSDN-10", 12, 3)
        };

        for (Producte_Dades p : productes) {
            db.store(p);
        }
    }

    public static void menu10(ObjectContainer db) {
        Predicate p = new Predicate<Producte_Dades>() {
            @Override
            public boolean match(Producte_Dades producte_dades) {       //Retorna tots els producte_dades
                return true;
            }
        };

        List<Producte_Dades> productes = db.query(p);

        for(Producte_Dades p_dades : productes) {
            System.out.println(p_dades.toString());
        }
    }

}
