package com.company;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Departament {

    public static void menu3(ObjectContainer db) {
        String email = "";
        String tlf = "";
        for (int i = 1; i < IKSRotarranConstants.DEPARTAMENTS.length; i++) {
            email = IKSRotarranConstants.DEPARTAMENTS[i] + "@IKSRotarran.ik";
            tlf = "";
            for (int j = 0; j < 9; j++) {
                tlf += String.valueOf(i);
            }

            db.store(new Departament_Dades(i, IKSRotarranConstants.DEPARTAMENTS[i], email, tlf));
        }
    }

    public static void menu4(ObjectContainer db) {
        //Busca objectes Department Dades (iterar dept dades)
        Predicate p = new Predicate<Departament_Dades>() {          //Junta objetos del tipo Dept dades
            @Override
            public boolean match(Departament_Dades d) {             //Busca objetos del tipo Dept dades
                return true;
            }
        };

        ObjectSet<Departament_Dades> departaments_dades = db.query(p);

        while (departaments_dades.hasNext()) {

            Departament_Dades deptTmp = departaments_dades.next();
            //Mirem dins dels encàrrecs de cada dept els que té per assignar-los
            Predicate p2 = new Predicate<Encarrec_Dades>() {
                @Override
                public boolean match(Encarrec_Dades e) {            //Si encuentras uno...
                    return e.getIdDepartamentQueElDemana() == deptTmp.getId() || e.getIdDepartamentQueElDemana() == 0;
                    //Lo guarda si el departamento que lo pide es el mismo que la id del temporal
                    //OR
                    //Si el departamento que lo pide es == 0
                }
            };

            ObjectSet<Encarrec_Dades> encarrecsAlDeptTmp = db.query(p2);

            while (encarrecsAlDeptTmp.hasNext()) {
                Encarrec_Dades encarrecTmp = encarrecsAlDeptTmp.next();

                deptTmp.afegirEncarrec(encarrecTmp);
            }

            db.store(deptTmp);

        }
    }

    public static void menu12(ObjectContainer db) {
        Predicate p = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades departament_dades) {
                return true;
            }
        };

        List<Departament_Dades> departaments = db.query(p);

        for(Departament_Dades d_dades : departaments) {
            System.out.println(d_dades.getId() + ". " + d_dades.getNom());
        }
    }

    public static void menu13(ObjectContainer db) {
        Predicate p = new Predicate<Departament_Dades>() {
            @Override
            public boolean match(Departament_Dades departament_dades) {
                return true;
            }
        };

        List<Departament_Dades> departaments = db.query(p);

        for(Departament_Dades d_dades : departaments) {
            System.out.println(d_dades);
        }
    }

    public static void menu14(ObjectContainer db) {
        Predicate p = new Predicate<Producte_Dades>() {
            @Override
            public boolean match(Producte_Dades producte_dades) {
                return true;
            }
        };

        List <Producte_Dades> productesDesordenats = db.query(p);

        ArrayList<Producte_Dades> productes = new ArrayList<>();
        productes.addAll(productesDesordenats);
        Collections.sort(productes);

        System.out.println("Llista de productes:\n");
        for (int i = 0; i < productes.size(); i++) {
            System.out.println(i + ": " + productes.get(i).getProducteNom() + " (" + productes.get(i).getProductePreu() + " darseks)");
        }

        Scanner sc = new Scanner(System.in);
        boolean check = false;
        int position = 0;

        System.out.println("\nSelect one of the options above: ");
        while (!check) {
            if (sc.hasNextInt()) {
                position = sc.nextInt();
                if (position >= 0 && position < productes.size()) {
                    check = true;
                } else {
                    System.out.println("El producte no existeix.");
                }
            } else {
                System.out.println("Opció invàlida.");
            }
        }

        Predicate p2 = new Predicate<Encarrec_Dades>() {
            @Override
            public boolean match(Encarrec_Dades encarrec_dades) {
                return true;
            }
        };

        Producte_Dades prodtmp = productes.get(position);

        List<Encarrec_Dades> encarrecs = db.query(p2);
        int count = 0;

        for (Encarrec_Dades e : encarrecs) {
            Predicate p3 = new Predicate<Departament_Dades>() {
                @Override
                public boolean match(Departament_Dades departament_dades) {
                    return departament_dades.getLlistaEncarrecs().contains(prodtmp);
                }
            };

            ObjectSet<Departament_Dades> deparaments = db.query(p3);
            String result = "";

            if (deparaments.size() == 0) {
                result = "Trobats 0 encàrrecs pel producte: \"" + productes.get(position).getProducteNom() + "\".";
            } else {
                result = "Trobats " + count + " encàrrecs pel producte: \"" + productes.get(position).getProducteNom() + "\".\n";
                for (Departament_Dades dept : deparaments) {
                    System.out.println(result += "\n" + dept.toString());
                }
            }

            System.out.println(result);
        }
    }

}
